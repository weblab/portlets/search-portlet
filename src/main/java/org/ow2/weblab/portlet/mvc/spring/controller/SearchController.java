/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.mvc.spring.controller;

import java.util.LinkedList;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;
import javax.validation.Valid;
import javax.xml.namespace.QName;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.services.searcher.SearchArgs;
import org.ow2.weblab.core.services.searcher.SearchReturn;
import org.ow2.weblab.portlet.business.bean.AdvancedSearchBean;
import org.ow2.weblab.portlet.business.bean.QueryBean;
import org.ow2.weblab.portlet.business.exception.SearchQueryException;
import org.ow2.weblab.portlet.business.service.impl.SearchBusinessServices;
import org.ow2.weblab.portlet.business.validator.AdvancedSearchValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * Search portlet controller based on the Spring MVC Porltet framework.
 * Process request for the VIEW portlet mode.
 *
 * @author emilienbondu, gdupont
 *
 */

@Controller
@RequestMapping(value="VIEW")
@SessionAttributes({"user_last_query", "advancedSearchBean","reset"})
public class SearchController {

	private Log logger = LogFactory.getLog(SearchController.class);

	@Autowired
	private SearchBusinessServices service;

	@InitBinder("advancedSearchBean")
    protected void initBinder(WebDataBinder binder) {
		// add a custom validator
        binder.setValidator(new AdvancedSearchValidator());
    }

	//#############################################################################
	//					  render mapping methods								  #
	//#############################################################################
	@RenderMapping()
	public ModelAndView showView(ModelMap model, SessionStatus status) {
		ModelAndView mav = new ModelAndView("view");
		this.logger.debug("do view: "+model);
		// TODO: model not reset
		if (!model.containsAttribute("reset")) {
			if (model.containsAttribute("user_last_query")) {
				// fill query bean from the current user query
				model.addAttribute("user_last_search_beans", this.service.asBeans((Query)model.get("user_last_query")));
			}
			mav.addAllObjects(model);
		} else {
			// reset, cleaning session
			model.clear();
			this.logger.debug("clearing querybeans");
			model.addAttribute("user_last_search_beans", new LinkedList<QueryBean>());
			mav.addAllObjects(model);
			status.setComplete();
		}
		return mav;
	}

	@RenderMapping(params="action=go_advanced_search")
	public ModelAndView showAdvancedView(ModelMap model) {
		ModelAndView mav = new ModelAndView("advanced");

		if (!model.containsAttribute("advancedSearchBean")) {
			// injecting a default advanced search bean
			model.addAttribute("advancedSearchBean", new AdvancedSearchBean());
		}
		mav.addAllObjects(model);
		return mav;
	}

	//#############################################################################
	//					  action mapping methods								  #
	//#############################################################################
	@ActionMapping(value="do_search")
	public void doSearch(ActionResponse response,@RequestParam("queryInput") String query, ModelMap model) throws SearchQueryException {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("receiving action do_search");
		}

		Query queryToExecute = null;
		if (model.containsAttribute("user_last_query")) {
			// update the existing user query
			queryToExecute = this.service.updateQuery((Query)model.get("user_last_query"), query);
		} else {
			// create a new user query
			queryToExecute = this.service.createQuery(query);
		}

		// update model
		model.addAttribute("user_last_query", queryToExecute);

		// launch event to broadcast the searchArgs to all portlets
		// (searchportlet should catch the event itself)
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("publishing searchArgs in event : [{http://weblab.ow2.org/portlet/action}publishQuery]");
		}
		try{
			response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/action}publishQuery"), this.service.prepareSearchArg(queryToExecute));
		}catch(WebLabCheckedException wce){
			this.logger.error("Query search returned an exception");
			throw new SearchQueryException(wce.getMessage(), wce, queryToExecute);
		}
	}

	@ActionMapping(value="do_adv_search")
	public void doAdvSearch(ActionRequest req, ActionResponse response,@Valid @ModelAttribute AdvancedSearchBean advancedSearchBean, BindingResult bindingResult) throws WindowStateException, WebLabCheckedException {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("An expected action has been received [do_advanced_search].");
		}

		if (bindingResult.hasErrors()) {
			// return to the advanced search view
			response.setWindowState(WindowState.MAXIMIZED);
			response.setRenderParameter("action", "go_advanced_search");
		} else {
			// create query
			final ComposedQuery metaQuery = this.service.createAdvQueryFromBean(advancedSearchBean);

			// launch event to broadcast the searchArgs to all portlets
			// (searchportlet should catch the event itself)
			response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/action}publishQuery"), this.service.prepareSearchArg(metaQuery));
		}
	}

	@ActionMapping(value="do_save")
	public void doSave(ActionResponse res, ModelMap model) {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("Saving query: " + ((Query)model.get("user_last_query")).getUri());
		}

		if (model.containsAttribute("user_last_query")) {
			// send an event to save the query
			res.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/action}saveQuery"), (Query)model.get("user_last_query"));
		} else {
			this.logger.warn("No query launched, unable to save something.");
		}
	}

	@ActionMapping(value="do_reset")
	public void doReset(ActionResponse res, ModelMap model) {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("Do Reset.");
		}

		// adding the reset attribute
		model.addAttribute("reset", Boolean.TRUE);

		// sending a empty RS to reset others portlets
		// TODO send a reset event should be better
		final ResultSet emptyRS = WebLabResourceFactory.createResource("EmptyResulSet", "DummyReset", ResultSet.class);
		res.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/action}resetSearch"), emptyRS);
	}

	@ActionMapping(value="remove_filter")
	public void removeFilter(@RequestParam("queryUri") String queryURI, ActionResponse res, ModelMap model) throws WebLabCheckedException {
		if (model.containsAttribute("user_last_query")) {

			// remove the filter to user query
			Query query = this.service.removeFilter((Query)model.get("user_last_query"), queryURI);
			if(query == null){
				if (this.logger.isDebugEnabled()) {
					this.logger.debug("Asking for current query removal: that's a reset...");
				}

				// remove implies reset
				model.addAttribute("reset", Boolean.TRUE);

				final ResultSet emptyRS = WebLabResourceFactory.createResource("EmptyResulSet", "DummyReset", ResultSet.class);
				res.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/action}resetSearch"), emptyRS);
			} else {

				// update model
				model.addAttribute("user_last_query", query);
				if (this.logger.isDebugEnabled()) {
					this.logger.debug("Filter removed, launching updated query...");
				}
				// execute the query and send results
				res.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/action}publishQuery"), this.service.prepareSearchArg(query));
			}
		}
	}

	//#############################################################################
	//					  event mapping methods								      #
	//#############################################################################
	@EventMapping(value="{http://weblab.ow2.org/portlet/reaction}query")
	public void executeQuery(EventRequest request, EventResponse response, ModelMap model) throws SearchQueryException  {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("Reaction: query.");
		}

		if ((request.getEvent().getValue() == null) || !(request.getEvent().getValue() instanceof SearchArgs)) {
			this.logger.error("The Event passed has no (or incorrect) value: [" + request.getEvent().getValue() + "]");
			//throw new WebLabCheckedException(SearchPortletConstants.SEARCH_ERROR, "The Event passed has no (or incorrect) value: [" + req.getEvent().getValue() + "]");
		}

		// update model
		SearchArgs searchArgs = (SearchArgs)request.getEvent().getValue();
		model.addAttribute("user_last_query", searchArgs.getQuery());

		// prepare search arg, execute the query and send results
		SearchReturn searchResult;
		try {
			searchResult = this.service.executeQuery(searchArgs, request.getRemoteUser(), "defaultContext");
			response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/action}search"), searchResult.getResultSet());
		} catch (WebLabCheckedException wce) {
			throw new SearchQueryException(wce.getMessage(), wce, searchArgs.getQuery());
		}
	}

	@EventMapping(value="{http://weblab.ow2.org/portlet/reaction}sendNextDocuments")
	public void sendNextDocuments(final EventRequest req, final EventResponse res, ModelMap model) throws WebLabCheckedException {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("Reaction: next page.");
		}

		if ((req.getEvent().getValue() != null) && (req.getEvent().getValue() instanceof PieceOfKnowledge)) {

			// get query URI from order
			String queryURI = this.service.getOrderedQueryURI((PieceOfKnowledge) req.getEvent().getValue());

			// checking if the portlet is the query producer
			if (model.containsAttribute("user_last_query") && ((Query)model.get("user_last_query")).getUri().equals(queryURI)) {
				// this portlet has generated the query, redo search new offset
				// and limit and send results
				SearchArgs args = this.service.prepareSearchArg((Query)model.get("user_last_query"), (PieceOfKnowledge) req.getEvent().getValue());
				final SearchReturn searchResult = this.service.executeQuery(args, req.getRemoteUser(), "defaultContext");
				res.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/action}nextDocuments"), searchResult.getResultSet());
			}
		}
	}

	//#############################################################################
	//					  exceptions handlers									  #
	//#############################################################################
	@ExceptionHandler(WebLabCheckedException.class)
	public ModelAndView handleException(WebLabCheckedException ex) {
		this.logger.error(ex.getLocalizedMessage(), ex);
		ModelAndView mav = new ModelAndView("view");
		if (ex instanceof SearchQueryException){
			Query query = ((SearchQueryException)ex).getQuery();
			this.logger.debug("Query returned an exception : "+this.service.asBeans(query));
			mav.addObject("user_last_search_beans", this.service.asBeans(query));
		}
		mav.addObject("error", Boolean.TRUE);
		mav.addObject("error_message", ex.getMessage());
		return mav;
	}
}
