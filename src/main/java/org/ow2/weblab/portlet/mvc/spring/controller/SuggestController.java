/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.mvc.spring.controller;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONValue;
import org.ow2.weblab.portlet.business.Constants;
import org.ow2.weblab.portlet.business.service.SuggestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

/**
 * Controller use to answer auto completion resource mapping.
 * Answer a JSON response to each query containing suggestions according to query user is currently typing in text input
 * @author lmartin
 *
 */
@Controller()
@RequestMapping(value = "VIEW")
public class SuggestController {

	@Autowired
	@Qualifier("suggestService")
	private SuggestService suggestService;

	// logger
	protected Log logger = LogFactory.getLog(SuggestController.class);

	@ResourceMapping(Constants.SUGGEST_RESOURCE)
	public void displayIFrame(
			@RequestParam(required = true, value = Constants.SUGGEST_CURRENT_INPUT_PARAM) String currentInput,
			@RequestParam(required = true, value = Constants.SUGGEST_CURRENT_VALUE_PARAM) String currentQuery,
			ResourceRequest request, ResourceResponse response) {
		this.logger.debug("Current query typed by user : "+currentInput+" = "+currentQuery);

		List<Map<String, String>> elements = this.suggestService.suggest(currentInput,currentQuery);

		//some debu logs
		try {
			StringWriter writer = new StringWriter();
			JSONValue.writeJSONString(elements, writer);
			this.logger.debug("return suggestions as JSON : \n"+writer.toString());
		} catch (IOException e1) {
			// ignored (just for logging)
		}

		//write corresponding JSON array as resource response
		try {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(JSONValue.toJSONString(elements));
			response.getWriter().flush();
		} catch (IOException e) {
			this.logger.error(e.getMessage(), e);
		}
	}

}
