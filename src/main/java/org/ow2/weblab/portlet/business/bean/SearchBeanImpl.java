/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.bean;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.annotator.BaseAnnotator;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.ontologies.DublinCore;
import org.ow2.weblab.core.extended.ontologies.RDFS;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.Operator;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.portlet.business.Constants;
import org.ow2.weblab.portlet.mvc.spring.controller.SearchController;
import org.ow2.weblab.rdf.Value;
import org.springframework.util.StringUtils;

/**
 * A {@link SearchBean} implementation. Propose user to launch "normal" text query or to use an advanced mode. This bean is managed in user's session via {@link SearchController}
 *
 * @see SearchBean
 * @author lmartin
 *
 */
public class SearchBeanImpl implements SearchBean {

	// form inputs user has to fill to launch a new query
	private String textQuery;
	// did user type an advanced SOLR query (iraq~ -> iraq, irak...)
	private boolean advancedQuery;
	// did user choose advanced mode
	private boolean advancedSearch;
	private HashMap<String, List<String>> filters;
	private HashMap<String, List<String>> notFilters;

	// advanced fields
	private String andKeywords;
	private String orKeywords;
	private String notKeywords;
	private String phraseKeywords;
	private String lang;
	private String format;
	private String startDate;
	private String endDate;

	// last query launched by current user
	private Query lastQuery;

	// order property used with current query (used to order results ascending/descending according to a specific field (hasGatheringDate...))
	private String orderProperty;
	private boolean orderAscendant;

	// bean use to configure some constants used in this search bean implementation
	protected ConfigBean configBean;

	final public Log logger = LogFactory.getLog(this.getClass());

	/**
	 * Create a new SearchBeanImpl instance (init filters and not filters with new empty lists
	 */
	public SearchBeanImpl() {
		// new filters (corresponding to facets)
		this.filters = new HashMap<>();
		this.notFilters = new HashMap<>();
	}

	@Override
	public Query buildQuery() {
		String mainQueryID = "searchQuery_" + UUID.randomUUID();
		this.logger.info("Build a new Query " + mainQueryID + " advanced: " + this.advancedSearch);

		ComposedQuery mainQuery = WebLabResourceFactory.createResource("searchPortlet", mainQueryID, ComposedQuery.class);
		mainQuery.setOperator(Operator.AND);

		// set order if needed
		if (StringUtils.hasText(this.orderProperty)) {
			WRetrievalAnnotator wra = new WRetrievalAnnotator(mainQuery);
			wra.writeToBeRankedBy(URI.create(this.orderProperty));
			wra.writeToBeRankedAscending(Boolean.valueOf(this.orderAscendant));
			this.logger.debug("Order: " + this.orderProperty + ", ascendant: " + this.orderAscendant);
		}

		// Add currently selected facets
		String filtersQueryID = mainQueryID + "_filters";
		ComposedQuery filtersQuery = WebLabResourceFactory.createResource("searchPortlet", filtersQueryID, ComposedQuery.class);
		filtersQuery.setOperator(Operator.AND);
		boolean filterAdded = false;
		for (String scope : this.filters.keySet()) {
			for (String req : this.filters.get(scope)) {
				this.logger.info("Facet filter :" + scope + " : " + req);
				addToComposedQuery(filtersQuery, req, scope, "queryID_" + UUID.randomUUID(), Constants.FILTER_COMMENT, false);
				filterAdded = true;
			}
		}
		if (filterAdded) {
			mainQuery.getQuery().add(filtersQuery);
		}
		// not filters
		String notFiltersQueryID = mainQueryID + "_not_filters";
		ComposedQuery notFiltersQuery = WebLabResourceFactory.createResource("searchPortlet", notFiltersQueryID, ComposedQuery.class);
		notFiltersQuery.setOperator(Operator.NOT);
		filterAdded = false;
		for (String scope : this.notFilters.keySet()) {
			for (String req : this.notFilters.get(scope)) {
				this.logger.info("Facet not filter :" + scope + " : " + req);
				addToComposedQuery(notFiltersQuery, req, scope, "queryID_" + UUID.randomUUID(), Constants.NOT_FILTER_COMMENT, false);
				filterAdded = true;
			}
		}
		if (filterAdded) {
			mainQuery.getQuery().add(notFiltersQuery);
		}

		// advanced search : use corresponding fields
		if (this.advancedSearch) {
			if (!getAndKeywords().isEmpty()) {
				ComposedQuery andQuery = WebLabResourceFactory.createResource("searchPortlet", "queryID_" + UUID.randomUUID() + "_andKeyWords", ComposedQuery.class);
				andQuery.setOperator(Operator.AND);
				boolean somethingAdded = false;
				String words[] = getAndKeywords().trim().split(" ");
				for (String word : words) {
					somethingAdded = true;
					addToComposedQuery(andQuery, word, null, "queryID_" + UUID.randomUUID() + "_andKeyWords", Constants.AND_WORDS_COMMENT, true);
				}
				if (somethingAdded) {
					mainQuery.getQuery().add(andQuery);
				}
			}

			if (!getPhraseKeywords().isEmpty()) {
				addToComposedQuery(mainQuery, "\"" + escapeQueryCulprits(getPhraseKeywords()) + "\"", null, "queryID_" + UUID.randomUUID() + "_phrase", Constants.PHRASE_COMMENT, true);
			}

			if (!getOrKeywords().isEmpty()) {
				ComposedQuery orQuery = WebLabResourceFactory.createResource("searchPortlet", "queryID_" + UUID.randomUUID() + "_orKeyWords", ComposedQuery.class);
				orQuery.setOperator(Operator.OR);
				boolean somethingAdded = false;
				String words[] = getOrKeywords().trim().split(" ");
				for (String word : words) {
					somethingAdded = true;
					addToComposedQuery(orQuery, word, null, "queryID_" + UUID.randomUUID() + "_orKeyWords", Constants.OR_WORDS_COMMENT, true);
				}
				if (somethingAdded) {
					mainQuery.getQuery().add(orQuery);
				}
			}

			if (!getNotKeywords().isEmpty()) {
				ComposedQuery notQuery = WebLabResourceFactory.createResource("searchPortlet", "queryID_" + UUID.randomUUID() + "_notKeyWords", ComposedQuery.class);
				notQuery.setOperator(Operator.NOT);
				boolean somethingAdded = false;
				String words[] = getNotKeywords().trim().split(" ");
				for (String word : words) {
					somethingAdded = true;
					addToComposedQuery(notQuery, word, null, "queryID_" + UUID.randomUUID() + "_notKeyWords", Constants.NOT_WORDS_COMMENT, true);
				}
				if (somethingAdded) {
					mainQuery.getQuery().add(notQuery);
				}
			}

			if (!getFormat().isEmpty()) {
				ComposedQuery formatQuery = WebLabResourceFactory.createResource("searchPortlet", "queryID_" + UUID.randomUUID() + "_formats", ComposedQuery.class);
				formatQuery.setOperator(Operator.OR);
				boolean somethingAdded = false;
				String formats[] = getFormat().trim().split(" ");
				for (String format_l : formats) {
					somethingAdded = true;
					addToComposedQuery(formatQuery, format_l, DublinCore.FORMAT_PROPERTY_NAME, "queryID_" + UUID.randomUUID() + "_formats", Constants.FORMAT_COMMENT, true);
				}
				if (somethingAdded) {
					mainQuery.getQuery().add(formatQuery);
				}
			}

			if (!getLang().isEmpty()) {
				ComposedQuery langQuery = WebLabResourceFactory.createResource("searchPortlet", "queryID_" + UUID.randomUUID() + "_lang", ComposedQuery.class);
				langQuery.setOperator(Operator.OR);
				boolean somethingAdded = false;
				String langs[] = getLang().trim().split(" ");
				for (String lang_l : langs) {
					somethingAdded = true;
					addToComposedQuery(langQuery, lang_l, DublinCore.LANGUAGE_PROPERTY_NAME, "queryID_" + UUID.randomUUID() + "_lang", Constants.LANG_COMMENT, true);
				}
				if (somethingAdded) {
					mainQuery.getQuery().add(langQuery);
				}
			}

			if ((StringUtils.hasText(getStartDate())) && (StringUtils.hasText(getEndDate()))) {
				final String request = '[' + getStartDate().trim() + "T00:00:00Z" + ' ' + getEndDate().trim() + "T23:59:59Z" + ']';
				addToComposedQuery(mainQuery, request, WebLabProcessing.HAS_GATHERING_DATE, "queryID_" + UUID.randomUUID() + "_dateQuery", Constants.DATE_COMMENT, false);

			}

			// clear potential text query that was hide in view
			this.textQuery = "";
			// "normal" search (not advanced)
		} else {
			if (StringUtils.hasText(this.textQuery)) {
				// text search input (escape only if user do not click "advanced" query)
				addToComposedQuery(mainQuery, this.textQuery, null, "queryID_" + UUID.randomUUID() + "_textQuery", Constants.QUERY_TEXT_COMMENT, !this.advancedQuery);
				if(this.advancedQuery) {
					new BaseAnnotator(mainQuery).applyOperator(org.ow2.weblab.core.annotator.AbstractAnnotator.Operator.WRITE, "rdfs", URI.create(RDFS.COMMENT), String.class, Constants.ADVANCED_COMMENT);
				}
			} else {
				this.textQuery = "";
			}
		}

		try {
			this.logger.trace("New built query : \n--------------------------------------\n" + ResourceUtil.saveToXMLString(mainQuery) + "\n---------------------------------------");
		} catch (WebLabCheckedException e) {
			this.logger.warn("An error occured when trying to marshall " + mainQuery.getUri());
		}

		// update last query
		this.lastQuery = mainQuery;
		return this.lastQuery;
	}

	@Override
	public Query getLastQuery() {
		if (this.lastQuery == null) {
			return buildQuery();
		}
		return this.lastQuery;
	}

	@Override
	public Query getInitQuery() {
		String request = this.configBean.getInitalStringQuery();
		// no init query to send -> return null
		if (!StringUtils.hasText(request)) {
			return null;
		}
		String initQueryID = "searchInitQuery_" + UUID.randomUUID();
		StringQuery initQuery = WebLabResourceFactory.createResource("searchPortlet", initQueryID, StringQuery.class);
		initQuery.setRequest(request);
		return initQuery;
	}

	@Override
	public void readQuery(Query query) throws WebLabCheckedException {
		// clear current fields
		clear();

		// //order by
		// values = hlpr.getRessOnPredSubj(query.getUri(), WebLabRetrieval.TO_BE_RANKED_BY);
		// if ((values != null) && (values.size() == 1)) {
		// orderProperty=values.get(0);
		// }

		// look for an "advanced" comment on this main query
		final Value<String> comments = new WRetrievalAnnotator(query).applyOperator(org.ow2.weblab.core.annotator.AbstractAnnotator.Operator.READ, "rdfs", URI.create(RDFS.COMMENT), String.class, null);
		this.advancedQuery = comments.hasValue() && comments.getValues().contains(Constants.ADVANCED_COMMENT);

		//"immediate" StringQuery case
		if(query instanceof StringQuery) {
			this.advancedSearch=false;
			this.textQuery=((StringQuery) query).getRequest();
			return;
		}

		//parse of composed Query
		internalRead(query.getUri(),query.getUri(),((ComposedQuery)query).getOperator(),query);

		//if one field set this bean in advanced mode we check to put "text" field in "and keywords" list
		if(this.advancedSearch&&(StringUtils.hasText(this.textQuery))) {
			this.andKeywords+=(StringUtils.hasText(this.andKeywords))?" ":""+this.textQuery;
			this.textQuery="";
		}
	}


	private void internalRead(String mainQueryURI, String parentQueryURI, Operator parentOperator,Query query) throws WebLabCheckedException {

		this.logger.debug("Start reading following query "+query.getUri()+"  -  Main Query: "+mainQueryURI+"  -  Current parent query: "+parentQueryURI+"  -  parent Operator:"+parentOperator);

		//StringQuery case
		if(query instanceof StringQuery) {
			//string request of this query
			String request=((StringQuery) query).getRequest();
			if(!StringUtils.hasText(request)) {
				throw new WebLabCheckedException("Error during query parsing.could not retrieve string request ("+request+").  -  Main Query: "+mainQueryURI+"  -  Current parent query: "+parentQueryURI+"  -  Current sub query: "+query.getUri());
			}

			// look for an "advanced" comment on this query -> there is one : advanced query is set to true
			final WRetrievalAnnotator wra = new WRetrievalAnnotator(query);
			final Value<String> comments = wra.applyOperator(org.ow2.weblab.core.annotator.AbstractAnnotator.Operator.READ, "rdfs", URI.create(RDFS.COMMENT), String.class, null);
			this.advancedQuery = comments.hasValue() && comments.getValues().contains(Constants.ADVANCED_COMMENT);

			//look at scope to retrieve filter, not filter, date, lang or format

			Value<URI> scopes = wra.readScope();
			//There is a scope -> this is not a "simple" field
			if(scopes.hasValue()) {
				final String scope = scopes.firstTypedValue().toASCIIString();
				if (scopes.size() > 1) {
					this.logger.warn("More than one scope found on query " + query.getUri() +".");
				}
				this.logger.debug("Found scope "+scope+" in following query: "+query.getUri());
				if(scope.equals(DublinCore.LANGUAGE_PROPERTY_NAME)) {
					//1st case of lang scope : fill lang field -> else : add a new filter
					if(parentOperator==Operator.OR) {
						this.advancedSearch=true;
						this.lang+=StringUtils.hasText(this.lang)?" ":""+((StringQuery) query).getRequest();
						this.logger.debug("Init lang field: "+this.lang);
						return;
					}else if(parentOperator==Operator.AND){
						addFilter(scope, request);
						this.logger.debug("Add filter: "+scope+":"+request);
						return;
					}else if(parentOperator==Operator.NOT){
						addNotFilter(scope, request);
						this.logger.debug("Add NOT filter: "+scope+":"+request);
						return;
					}
				}else if(scope.equals(DublinCore.FORMAT_PROPERTY_NAME)) {
					//1st case of lang scope : fill lang field -> else : add a new filter
					if(parentOperator==Operator.OR) {
						this.advancedSearch=true;
						this.format=((StringQuery) query).getRequest();
						this.format+=StringUtils.hasText(this.format)?" ":""+((StringQuery) query).getRequest();
						this.logger.debug("Init format field: "+this.format);
						return;
					}else if(parentOperator==Operator.AND){
						addFilter(scope, request);
						this.logger.debug("Add filter: "+scope+":"+request);
						return;
					}else if(parentOperator==Operator.NOT){
						addNotFilter(scope, request);
						this.logger.debug("Add NOT filter: "+scope+":"+request);
						return;
					}
				}else if(scope.equals(WebLabProcessing.HAS_GATHERING_DATE)) {
					//1st case of lang scope : fill lang field -> else : add a new filter
					boolean addAsFilter=false;
					if((!StringUtils.hasText(this.startDate))&&(parentOperator==Operator.AND)) {
						String[] dates = request.split(" |\\[|\\]|T");
						if(dates.length<5) {
							this.logger.warn("Could not retrieve start and end gathering dates correctly: "+request+"  -  Main Query: "+mainQueryURI+"  -  Current parent query: "+parentQueryURI+"  -  Current sub query: "+query.getUri());
							addAsFilter=true;
						}else {
							this.startDate = dates[1];
							this.endDate = dates[3];
							this.advancedSearch=true;
							this.logger.debug("Init dates fields: "+this.startDate+" - "+this.endDate);
							return;
						}
					}
					//start and end dates was already filled OR curent request could not be splitted correctly into start / end dates -> add this scope as filter
					if(addAsFilter) {
						if(parentOperator==Operator.AND){
							addFilter(scope, request);
							this.logger.debug("Add filter: "+scope+":"+request);
							return;
						}else if(parentOperator==Operator.NOT){
							addNotFilter(scope, request);
							this.logger.debug("Add NOT filter: "+scope+":"+request);
							return;
						}
					}

				//Last "scope" case -> filters
				}else if(parentOperator==Operator.AND){
					addFilter(scope, request);
					this.logger.debug("Add filter: "+scope+":"+request);
					return;
				}else if(parentOperator==Operator.NOT){
					addNotFilter(scope, request);
					this.logger.debug("Add NOT filter: "+scope+":"+request);
					return;
				}
			//NO scope case
			}else {
				if(parentOperator==Operator.AND){
					//specific case : phrase starts and ends with "
					if(request.startsWith("\"")&&request.endsWith("\"")) {
						this.phraseKeywords=request.substring(1,request.length()-1);
						this.advancedSearch=true;
						this.logger.debug("Init phrase field: "+this.phraseKeywords);
						return;
					}
					//no text query for the moment -> we init it
					if((!StringUtils.hasText(this.andKeywords))&&(!StringUtils.hasText(this.textQuery))) {
						this.textQuery=request;
						this.logger.debug("Init text query field: "+this.textQuery);
						return;
					//there was a text query and we found a new WORD -> clear text query, set advanced search to true and add this new request as "AND" word
					}
					this.advancedSearch=true;
					this.andKeywords+=((StringUtils.hasText(this.andKeywords))?" ":"")+this.textQuery;
					this.andKeywords+=" "+request;
					this.textQuery="";
					this.logger.debug("Add AND keyword: "+request+" ("+this.andKeywords+")");
				}else if(parentOperator==Operator.OR) {
					//no text query for the moment -> we init it
					if((!StringUtils.hasText(this.orKeywords))&&(!StringUtils.hasText(this.textQuery))) {
						this.textQuery=request;
						this.logger.debug("Init text query field: "+this.textQuery);
						return;
					//there was a text query and we found a new WORD -> clear text query, set advanced search to true and add this new request as "AND" word
					}
					this.advancedSearch=true;
					this.orKeywords+=((StringUtils.hasText(this.orKeywords))?" ":"")+this.textQuery;
					this.orKeywords+=" "+request;
					this.textQuery="";
					this.logger.debug("Add OR keyword: "+request+" ("+this.orKeywords+")");
				}else if(parentOperator==Operator.NOT) {
					//no text query for the moment -> we init it
					if((!StringUtils.hasText(this.notKeywords))&&(!StringUtils.hasText(this.textQuery))) {
						this.textQuery=request;
						this.logger.debug("Init text query field: "+this.textQuery);
						return;
					//there was a text query and we found a new WORD -> clear text query, set advanced search to true and add this new request as "AND" word
					}
					this.advancedSearch=true;
					this.notKeywords+=((StringUtils.hasText(this.notKeywords))?" ":"")+this.textQuery;
					this.notKeywords+=" "+request;
					this.textQuery="";
					this.logger.debug("Add NOT keyword: "+request+" ("+this.notKeywords+")");
				}
			}
			//that's it for StringQuery
			return;
		//Composed Query case : look at its children
		} else if (query instanceof ComposedQuery) {
			for (Query subQuery : ((ComposedQuery) query).getQuery()) {
				internalRead(mainQueryURI, query.getUri(), ((ComposedQuery) query).getOperator(), subQuery);
			}
		}

	}

	public void clear() {
		// form inputs user has to fill to launch a new query
		this.textQuery = "";
		this.advancedQuery = false;
		this.advancedSearch = false;
		this.filters.clear();
		this.notFilters.clear();
		this.andKeywords = "";
		this.orKeywords = "";
		this.notKeywords = "";
		this.phraseKeywords = "";
		this.lang = "";
		this.format = "";
		this.startDate = "";
		this.endDate = "";
		this.lastQuery = null;
		this.orderProperty = "";
		this.orderAscendant = false;
	}

	/**
	 * @return the textQuery
	 */
	public String getTextQuery() {
		return this.textQuery;
	}

	/**
	 * @param textQuery the textQuery to set
	 */
	public void setTextQuery(String textQuery) {
		this.textQuery = textQuery;
	}

	public void addFilter(String scope, String request) {
		List<String> requests = this.filters.get(scope);
		if (requests == null) {
			requests = new ArrayList<>();
			this.filters.put(scope, requests);
		}
		if (!requests.contains(request)) {
			this.logger.info("Add filter " + scope + ":" + request);
			requests.add(request);
		}
	}

	public void addNotFilter(String scope, String request) {
		List<String> requests = this.notFilters.get(scope);
		if (requests == null) {
			requests = new ArrayList<>();
			this.notFilters.put(scope, requests);
		}
		if (!requests.contains(request)) {
			this.logger.info("Add NOT filter " + scope + ":" + request);
			requests.add(request);
		}
	}

	public void removeFilter(String scope, String filter) {
		List<String> fs = this.filters.get(scope);
		if (fs != null) {
			this.logger.info("Remove filter " + scope + ":" + filter);
			fs.remove(filter);
		}
	}

	public void removeNotFilter(String scope, String filter) {
		List<String> fs = this.notFilters.get(scope);
		if (fs != null) {
			this.logger.info("Remove NOT filter " + scope + ":" + filter);
			fs.remove(filter);
		}
	}

	/**
	 * @return the filters
	 */
	public HashMap<String, List<String>> getFilters() {
		return this.filters;
	}

	/**
	 * @param filters the filters to set
	 */
	public void setFilters(HashMap<String, List<String>> filters) {
		this.filters = filters;
	}

	/**
	 * @return the notFilters
	 */
	public HashMap<String, List<String>> getNotFilters() {
		return this.notFilters;
	}

	/**
	 * @param notFilters the notFilters to set
	 */
	public void setNotFilters(HashMap<String, List<String>> notFilters) {
		this.notFilters = notFilters;
	}

	/**
	 * @return the orderProperty
	 */
	public String getOrderProperty() {
		return this.orderProperty;
	}

	/**
	 * @param orderProperty the orderProperty to set
	 */
	public void setOrderProperty(String orderProperty) {
		this.orderProperty = orderProperty;
	}

	/**
	 * @return the orderAscendant
	 */
	public boolean isOrderAscendant() {
		return this.orderAscendant;
	}

	/**
	 * @param orderAscendant the orderAscendant to set
	 */
	public void setOrderAscendant(boolean orderAscendant) {
		this.orderAscendant = orderAscendant;
	}

	/**
	 * @return the configBean
	 */
	public ConfigBean getConfigBean() {
		return this.configBean;
	}

	/**
	 * @param configBean the configBean to set
	 */
	public void setConfigBean(ConfigBean configBean) {
		this.configBean = configBean;
	}

	/**
	 * @return the andKeywords
	 */
	public String getAndKeywords() {
		return this.andKeywords;
	}

	/**
	 * @param andKeywords the andKeywords to set
	 */
	public void setAndKeywords(String andKeywords) {
		this.andKeywords = andKeywords;
	}

	/**
	 * @return the orKeywords
	 */
	public String getOrKeywords() {
		return this.orKeywords;
	}

	/**
	 * @param orKeywords the orKeywords to set
	 */
	public void setOrKeywords(String orKeywords) {
		this.orKeywords = orKeywords;
	}

	/**
	 * @return the notKeywords
	 */
	public String getNotKeywords() {
		return this.notKeywords;
	}

	/**
	 * @param notKeywords the notKeywords to set
	 */
	public void setNotKeywords(String notKeywords) {
		this.notKeywords = notKeywords;
	}

	/**
	 * @return the phraseKeywords
	 */
	public String getPhraseKeywords() {
		return this.phraseKeywords;
	}

	/**
	 * @param phraseKeywords the phraseKeywords to set
	 */
	public void setPhraseKeywords(String phraseKeywords) {
		this.phraseKeywords = phraseKeywords;
	}

	/**
	 * @return the lang
	 */
	public String getLang() {
		return this.lang;
	}

	/**
	 * @param lang the lang to set
	 */
	public void setLang(String lang) {
		this.lang = lang;
	}

	/**
	 * @return the format
	 */
	public String getFormat() {
		return this.format;
	}

	/**
	 * @param format the format to set
	 */
	public void setFormat(String format) {
		this.format = format;
	}

	/**
	 * @return the startDate
	 */
	public String getStartDate() {
		return this.startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return this.endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @param lastQuery the lastQuery to set
	 */
	public void setLastQuery(Query lastQuery) {
		this.lastQuery = lastQuery;
	}

	/**
	 * @return the advancedQuery
	 */
	public boolean isAdvancedQuery() {
		return this.advancedQuery;
	}

	/**
	 * @param advancedQuery the advancedQuery to set
	 */
	public void setAdvancedQuery(boolean advancedQuery) {
		this.advancedQuery = advancedQuery;
	}

	/**
	 * @return the advancedSearch
	 */
	public boolean isAdvancedSearch() {
		return this.advancedSearch;
	}

	/**
	 * @param advancedSearch the advancedSearch to set
	 */
	public void setAdvancedSearch(boolean advancedSearch) {
		this.advancedSearch = advancedSearch;
	}

	/*
	 * Internal use : Add a query from string representation to an existing WebLab composed query
	 *
	 * @param metaQuery the query to modify
	 *
	 * @param request to add to the query
	 *
	 * @param scope of the request
	 */
	static protected void addToComposedQuery(final ComposedQuery metaQuery, String request, String scope, String tmpQueryIDRef, String comment, boolean escape) {
		final StringQuery q = WebLabResourceFactory.createResource(tmpQueryIDRef, "queryID_" + UUID.randomUUID(), StringQuery.class);

		final WRetrievalAnnotator wra = new WRetrievalAnnotator(q);
		if (scope != null) {
			wra.writeScope(URI.create(scope));
		}
		q.setRequest(escape ? escapeQueryCulprits(request) : request);
		wra.applyOperator(org.ow2.weblab.core.annotator.AbstractAnnotator.Operator.WRITE, "rdfs", URI.create(RDFS.COMMENT), String.class, comment);
		metaQuery.getQuery().add(q);

		LogFactory.getLog(SearchBeanImpl.class).debug("Add " + scope + ":" + request + " to query" + metaQuery.getUri() + " (Operator : " + metaQuery.getOperator() + ")");
	}

	public static String escapeQueryCulprits(String s) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			// These characters are part of the query syntax and must be escaped
			if (c == '\\' || c == '+' || c == '-' || c == '!' || c == '(' || c == ')' || c == ':' || c == '^' || c == '[' || c == ']' || c == '\"' || c == '{' || c == '}' || c == '~' || c == '*'
					|| c == '?' || c == '|' || c == '&' || c == ';') {
				sb.append('\\');
			}
			if (Character.isWhitespace(c)) {
				sb.append(" \\ ");
			}
			sb.append(c);
		}
		return sb.toString();
	}
}