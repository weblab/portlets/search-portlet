/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.bean;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.portlet.business.service.SearchService;
import org.ow2.weblab.portlet.mvc.spring.controller.SearchController;

/**
 * Bean managed by this portlet used to store all informations concerning current search
 * Such bean is managed in {@link SearchController} using {@link SearchService}
 * @author lmartin
 *
 */
public interface SearchBean {

	/**
	 * Build a new query from this bean's search fields
	 * @return {@link Query} a new query corresponding to this bean's current search fields
	 */
	public Query buildQuery();

	/**
	 * Get inital query from this bean's search fields. If null nothing will happen in controller. Else this initial query will be launch when user arrive on search page
	 * @return {@link Query} a new query corresponding to this bean's search fields initial values. If null nothing will happen in controller.
	 */
	public Query getInitQuery();

	/**
	 * Get last query launched by current user
	 * @return {@link Query} last query launched by current user
	 */
	public Query getLastQuery();

	/**
	 * Set this bean's search fields values according to a query
	 * @param query {@link Query} query used to set this bean's search fields
	 * @throws WebLabCheckedException
	 */
	public void readQuery(Query query) throws WebLabCheckedException;

}
