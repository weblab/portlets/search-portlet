/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.components.client.WebLabClient;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.ComposedResource;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.portlet.business.bean.ConfigBean;
import org.ow2.weblab.portlet.business.service.SuggestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class SuggestServiceImpl implements SuggestService {

	@Autowired
	@Qualifier("configBean")
	protected ConfigBean configBean;

	// logger used by all meta view portlet classes
	protected Log logger = LogFactory.getLog(SuggestServiceImpl.class);

	@Override
	public List<Map<String, String>> suggest(String inputName,String inputValue) {
		ArrayList<Map<String, String>> elements = new ArrayList<>();

		//call analyser service responsible of spell suggestions
		try {
			Analyser analyser= WebLabClient.getAnalyser(this.configBean.getAnalyserUserURI(), this.configBean.getAnalyserUsageContext(), this.configBean.getAnalyserServiceURI());
			this.logger.info("new suggestion with analyser service : " + analyser);

			StringQuery query = WebLabResourceFactory.createResource("searchPortlet", UUID.randomUUID()+"", StringQuery.class);
			query.setRequest(inputValue);
			ProcessArgs processArgs=new ProcessArgs();
			processArgs.setResource(query);
			ProcessReturn processReturn = analyser.process(processArgs);
			if(processReturn!=null) {
				Resource resource=processReturn.getResource();
				try {
					this.logger.trace("SuggestService response \n--------------------------------------\n" + ResourceUtil.saveToXMLString(resource) + "\n---------------------------------------");
				} catch (WebLabCheckedException e) {
					this.logger.warn("An error occured trying to marshall resource " + resource.getUri());
				}
				if(resource instanceof ComposedResource) {
					ComposedResource compResource=(ComposedResource)resource;
					List<Resource>resources=compResource.getResource();
					if((resources!=null)&&(!resources.isEmpty())) {
						for(Resource res : resources) {
							if(res instanceof StringQuery) {
								String q=((StringQuery) res).getRequest();
								this.logger.debug("Suggestion: "+res);
								Map<String, String> element = new HashMap<>();
								element.put("data", q);
								element.put("value", q);
								elements.add(element);
							}else {
								this.logger.warn("analyser.process this resource is not a String query: "+resource);
							}
						}
					}else {
						this.logger.warn("return ComposedResource do not contain any resource");
					}
				}else {
					this.logger.warn("analyser.process do not return a composed resource : "+resource);
				}
			}else {
				this.logger.warn("analyser.process return null");
			}
		} catch (Exception e) {
			this.logger.error(e.getMessage(),e);
		}
		return elements;
	}

}
