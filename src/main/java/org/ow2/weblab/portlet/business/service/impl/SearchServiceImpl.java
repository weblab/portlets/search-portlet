/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.service.impl;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.components.client.WebLabClient;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.services.Searcher;
import org.ow2.weblab.core.services.searcher.SearchArgs;
import org.ow2.weblab.core.services.searcher.SearchReturn;
import org.ow2.weblab.portlet.business.bean.ConfigBean;
import org.ow2.weblab.portlet.business.bean.SearchBeanImpl;
import org.ow2.weblab.portlet.business.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.StringUtils;

/**
 * {@link SearchService} implementation
 *
 * @author lmartin
 *
 */
public class SearchServiceImpl implements SearchService<SearchBeanImpl> {

	@Autowired
	@Qualifier("configBean")
	protected ConfigBean configBean;

	// logger used by all meta view portlet classes
	final static public Log LOGGER = LogFactory.getLog("search-portlet");

	@Override
	public SearchBeanImpl createSearchBean(PortletRequest request,PortletResponse response) {
		return new SearchBeanImpl();
	}

	@Override
	public void resetSearchBean(SearchBeanImpl searchBean) {
		//nothing to do in this implementation
	}

	@Override
	public Query prepareQuery(SearchBeanImpl searchBean) {
		return searchBean.buildQuery();
	}

	@Override
	public Query getInitQuery(SearchBeanImpl searchBean) {
		return searchBean.getInitQuery();
	}

	@Override
	public SearchReturn executeQuery(SearchBeanImpl searchBean, SearchArgs searchArgs) throws WebLabCheckedException {
		Searcher service = null;
		try {
			service = WebLabClient.getSearcher(this.configBean.getSearcherUserURI(), this.configBean.getSearcherUsageContext(), this.configBean.getSearcherServiceURI());
		} catch (WebLabCheckedException e) {
			throw new WebLabCheckedException(e.getMessage() + "(userURI: " + this.configBean.getSearcherUserURI() + ", usage context: " + this.configBean.getSearcherUsageContext() + ", searcherURI: "
					+ this.configBean.getSearcherServiceURI() + ")", e);

		}
		LOGGER.info("new search with search service : " + service);
		try {
			return service.search(searchArgs);
		} catch (Exception e) {
			throw new WebLabCheckedException(e.getMessage() + "(searcher: " + service + ")", e);
		}
	}

	@Override
	public Query updateBean(SearchBeanImpl searchBean, Query query) throws WebLabCheckedException {
		//reset search bean according to this new query
		searchBean.readQuery(query);
		return query;
	}

	@Override
	public boolean isEmpty(Query query) {
		if (query instanceof ComposedQuery) {
			return (((ComposedQuery) query).getQuery().size() == 0);
		} else if (query instanceof StringQuery) {
			return !StringUtils.hasText(((StringQuery) query).getRequest());
		}
		// this service implementation only managed Composed query or String query : other queries are considered as empty
		return true;
	}

	@Override
	public void addFilter(SearchBeanImpl searchBean, String scope, String request) {
		searchBean.addFilter(scope, request);
	}

	@Override
	public void addNotFilter(SearchBeanImpl searchBean, String scope, String request) {
		searchBean.addNotFilter(scope, request);
	}

	@Override
	public void removeFilter(SearchBeanImpl searchBean, String scope, String request) {
		searchBean.removeFilter(scope, request);
	}

	@Override
	public void removeNotFilter(SearchBeanImpl searchBean, String scope, String request) {
		searchBean.removeNotFilter(scope, request);
	}

	@Override
	public Query getLastQuery(SearchBeanImpl searchBean) {
		return searchBean.getLastQuery();
	}

	@Override
	public void addOrder(SearchBeanImpl searchBean, String orderProperty, boolean ascending) {
		searchBean.setOrderProperty(orderProperty);
		searchBean.setOrderAscendant(ascending);
	}
}
