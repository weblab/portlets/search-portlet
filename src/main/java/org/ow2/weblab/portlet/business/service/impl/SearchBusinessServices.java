/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.business.service.impl;

import java.net.MalformedURLException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.components.client.WebLabClient;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.ontologies.DublinCore;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.extended.util.PoKUtil;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.helper.impl.AdvancedSelector;
import org.ow2.weblab.core.helper.impl.RDFSelectorFactory;
import org.ow2.weblab.core.helper.impl.Statements;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.Operator;
import org.ow2.weblab.core.model.PieceOfKnowledge;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.core.services.Searcher;
import org.ow2.weblab.core.services.searcher.SearchArgs;
import org.ow2.weblab.core.services.searcher.SearchReturn;
import org.ow2.weblab.portlet.business.bean.AdvancedSearchBean;
import org.ow2.weblab.portlet.business.bean.ConfigBean;
import org.ow2.weblab.portlet.business.bean.QueryBean;
import org.ow2.weblab.rdf.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * The search portlet business service.
 * Offered services allow to deal with Weblab queries and Weblab search services
 *
 * @author emilienbondu, gdupont
 *
 */

@Service(value="searchBusinessService")
public class SearchBusinessServices {

	public static final String HAS_SCOPE = WebLabRetrieval.HAS_SCOPE;

	// attributes
	public static final String DEFAULT_USER_URI = "http://weblab.ow2.org/users#weblab";
	public static final String DEFAULT_USAGE_CONTEXT = "http://weblab.ow2.org/portlets#SearchPortlet";
	public static final String USER_URI_PREFIX = "http://weblab.ow2.org/users#";

	protected Log logger = LogFactory.getLog(this.getClass());

	@Autowired()
	protected ConfigBean configuration;

	/**
	 * To create a WebLab query from a string representation of the user query
	 * @param query
	 * @return the WL query
	 */
	public Query createQuery(String query) {
		final StringQuery stringQuery = WebLabResourceFactory.createResource("http://searchPortlet", "queryID_"+ UUID.randomUUID(), StringQuery.class);
		stringQuery.setRequest(query);
		return stringQuery;
	}

	/**
	 * To create a WebLab Composed query from an advanced search bean filled by the user
	 * @param advancedSearchBean
	 * @return the composed query
	 */
	public ComposedQuery createAdvQueryFromBean(AdvancedSearchBean advancedSearchBean) {
		final ComposedQuery metaQuery = WebLabResourceFactory.createResource("http://searchPortlet", "queryID_"+ UUID.randomUUID(), ComposedQuery.class);

		metaQuery.setOperator(Operator.AND);

		if (!advancedSearchBean.getAndKeywords().isEmpty()) {
			final String request = advancedSearchBean.getAndKeywords().trim().replaceAll(" ", " " + Operator.AND + " ");
			addToComposedQuery(metaQuery, request, null);
		}

		if (!advancedSearchBean.getPhraseKeywords().isEmpty()) {
			final String request = "\""+advancedSearchBean.getPhraseKeywords().trim().replaceAll("\"", "\"")+"\"";
			addToComposedQuery(metaQuery, request, null);
		}

		if (!advancedSearchBean.getOrKeywords().isEmpty()) {
			final String request ='(' + advancedSearchBean.getOrKeywords().trim().replaceAll(" ", " " + Operator.OR + " ")+ ')';
			addToComposedQuery(metaQuery, request, null);
		}

		if (!advancedSearchBean.getNotKeywords().isEmpty()) {
			final String request = "" + Operator.NOT + ' ' + "(" + advancedSearchBean.getNotKeywords().trim().replaceAll(" ", " " + Operator.OR + " ") + ')';
			addToComposedQuery(metaQuery, request, null);
		}

		if (!advancedSearchBean.getFormat().isEmpty()) {
			final String[] request_parts = advancedSearchBean.getFormat().trim().split(" ");
			final StringBuffer request = new StringBuffer();

			int index = 0;
			for(String part:request_parts){
				if (!part.startsWith("application/")){
					request.append("application/");
				}
				request.append(part);
				if (index < request_parts.length-1){
					request.append(" ");
					request.append(Operator.OR.toString());
					request.append(" ");
				}
				index++;
			}


			addToComposedQuery(metaQuery, request.toString(), DublinCore.FORMAT_PROPERTY_NAME);
		}

		if (!advancedSearchBean.getLang().isEmpty()) {
			final String request = advancedSearchBean.getLang().trim().replaceAll(" ", " " + Operator.OR + " ");
			addToComposedQuery(metaQuery, request, DublinCore.LANGUAGE_PROPERTY_NAME);
		}

		if (advancedSearchBean.getStartDate() != null && advancedSearchBean.getStartDate() != "" && advancedSearchBean.getEndDate() != null && advancedSearchBean.getEndDate() != "") {
			final String request = '[' + advancedSearchBean.getStartDate().trim() + "T00:00:00Z" + ' ' + advancedSearchBean.getEndDate().trim() + "T23:59:59Z"
					+ ']';
			addToComposedQuery(metaQuery, request, WebLabProcessing.HAS_GATHERING_DATE);
		}
		return metaQuery;
	}

	/**
	 * Adding new query as extra filter in existing composed query if any
	 * @param originalQuery the original query
	 * @param query new query inputs
	 * @return updated query if necessary
	 */
	public Query updateQuery(Query originalQuery, String query) {
		if(originalQuery != null){
			if (originalQuery instanceof ComposedQuery) {
				ComposedQuery cQuery = (ComposedQuery) originalQuery;
				cQuery.getQuery().add(createQuery(query));
				return cQuery;
			}
			ComposedQuery metaQuery = WebLabResourceFactory.createResource("http://searchPortlet", "queryID_"+ UUID.randomUUID(), ComposedQuery.class);
			metaQuery.setOperator(Operator.AND);
			metaQuery.getQuery().add(originalQuery);
			metaQuery.getQuery().add(createQuery(query));
			return metaQuery;
		}
		return originalQuery;
	}

	/**
	 * Add a query from string representation to an existing WebLab composed query
	 * @param metaQuery the query to modify
	 * @param request to add to the query
	 * @param scope of the request
	 */
	public void addToComposedQuery(final ComposedQuery metaQuery, String request, String scope) {
		final StringQuery q = WebLabResourceFactory.createResource("http://searchPortlet", "queryID_"+ UUID.randomUUID(), StringQuery.class);

		if (scope != null) {
			new WRetrievalAnnotator(q).writeScope(URI.create(scope));
		}
		q.setRequest(request);
		metaQuery.getQuery().add(q);
	}

	/**
	 * To remove a filter (identified by it URI) to the input WebLab query
	 * @param query
	 * @param queryURI
	 * @return the modified query of null if the modified query is an empty query
	 */
	public Query removeFilter(Query query, String queryURI) {
		if (query.getUri().equals(queryURI)) {
			return null;
		}

		if (query instanceof ComposedQuery) {
			ComposedQuery composedQuery = (ComposedQuery) query;
			Query toBeRemoved = null;
			for (Query q : composedQuery.getQuery()) {
				if (removeFilter(q, queryURI) == null) {
					if (this.logger.isDebugEnabled()) {
						this.logger.debug("Removing [" + queryURI + "] query from filters. Keeping the rest.");
					}
					toBeRemoved = q;
					break;
				}
			}
			if(toBeRemoved != null){
				composedQuery.getQuery().remove(toBeRemoved);
			}

			//TODO optimizing the nested composedQuery when necessary
			if(composedQuery.getQuery().size() == 1 && !Operator.NOT.equals(composedQuery.getOperator())) {
				query = composedQuery.getQuery().get(0);
			} else if (composedQuery.getQuery().size() == 0) {
				return null;
			}
			return query;
		}
		this.logger.warn("Unable to find query with uri: " + queryURI + ". Keeping current query.");
		return query;
	}

	/**
	 * To get a WebLab search arg for a Search service from the input WebLab query
	 * @param q
	 * @return The created search args
	 * @throws WebLabCheckedException
	 */
	public SearchArgs prepareSearchArg(Query q) throws WebLabCheckedException {
		if (q == null) {
			throw new WebLabCheckedException("Query is null");
		}

		// create args for search service
		final SearchArgs args = new SearchArgs();
		args.setLimit(Integer.valueOf(10));
		args.setOffset(Integer.valueOf(0));
		args.setQuery(q);
		return args;
	}

	/**
	 * To get a WebLab search arg from the input query and a search order describe in the given POK
	 * POK must to describe the expected offset and limit
	 *
	 * @param query
	 * @param pok
	 * @return the search arg
	 */
	public SearchArgs prepareSearchArg(Query query, PieceOfKnowledge pok) {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug(PoKUtil.getPoKData(pok));
		}
		final AdvancedSelector rdfSelector = RDFSelectorFactory.getSelector(true);
		final Statements map = rdfSelector.searchFor(pok);

		SearchArgs searchArg = new SearchArgs();
		searchArg.setQuery(query);

		searchArg.setOffset(map.getTypedValue(null, WebLabRetrieval.HAS_EXPECTED_OFFSET, Integer.class));
		searchArg.setLimit(map.getTypedValue(null, WebLabRetrieval.HAS_EXPECTED_LIMIT, Integer.class));

		if (this.logger.isDebugEnabled()) {
			this.logger.debug("Getting next SERP - offset/limit requested:" + searchArg.getOffset() + "/" + searchArg.getLimit());
		}
		return searchArg;
	}

	/**
	 * To execute a WebLab query by addressing it to the configured WebLab searcher service.
	 * @param args
	 * @param remoteUser
	 * @param context
	 * @return the result set
	 * @throws WebLabCheckedException
	 */
	public SearchReturn executeQuery(SearchArgs args, String remoteUser, String context) throws WebLabCheckedException {
		Searcher service = null;
		SearchReturn ret = null;
		if (remoteUser != null) {
			service = WebLabClient.getSearcher(remoteUser, context, this.configuration.getSearcherServiceURI());
		} else {
			service = WebLabClient.getSearcher(DEFAULT_USER_URI, context, this.configuration.getSearcherServiceURI());
		}

		if (this.logger.isDebugEnabled()) {
			this.logger.debug("Using WL service "+service+ "to get search with user:"+remoteUser);
		}
		try {
			ret = service.search(args);
		} catch (Exception e) {
			this.logger.error("error from search service, query offset is :"+args.getOffset()+" query limit is :"+args.getLimit()+"; query is "+asBeans(args.getQuery())+" . Error cause is:"+e.getMessage());
			throw new WebLabCheckedException(e.getMessage());
		}

		if (this.logger.isDebugEnabled()) {
			this.logger.debug("service response is :"+ResourceUtil.saveToXMLString(ret.getResultSet()));
		}
		return ret;
	}

	/**
	 * To get the URI of the query describe in the given search order describe in the POK
	 * @param pok
	 * @return the query URI
	 */
	public String getOrderedQueryURI(PieceOfKnowledge pok) {
		final AdvancedSelector rdfSelector = RDFSelectorFactory.getSelector(true);
		final Statements map = rdfSelector.searchFor(pok);
		final String queryURI = map.getFirstValue(null, WebLabRetrieval.HAS_ORDERED_QUERY, null);

		if (this.logger.isDebugEnabled()) {
			this.logger.debug("query URI :" + queryURI);
		}
		return queryURI;
	}

	/**
	 * describe the given query as a Query bean
	 * @param query
	 * @return The list of query beans
	 */
	public List<QueryBean> asBeans(Query query) {
		List<QueryBean> queryBeans = new ArrayList<>();
		fillBeans(query, queryBeans, null);
		return queryBeans;
	}

	/**
	 * Private and recursive void to fill a Query bean from a WebLab query
	 * @param query
	 * @param queryBeans
	 * @param operator
	 */
	private void fillBeans(Query query, List<QueryBean> queryBeans, Operator operator) {
		if (query instanceof StringQuery) {
			WRetrievalAnnotator wra = new WRetrievalAnnotator(query);
			StringQuery stringQuery = (StringQuery) query;
			String label = stringQuery.getRequest();
			Value<String> labels = wra.readLabel();
			if (labels.hasValue()) {
				label = labels.firstTypedValue();
			}
			String scope = "";
			Value<URI> scopes = wra.readScope();
			// TODO read an OWL file? Label form URI ? Another XML mapping file?
			if (scopes.hasValue()) {
				try {
					scope = scopes.firstTypedValue().toURL().getRef();
					if (scope == null || scope.trim().length() == 0) {
						scope = scopes.firstTypedValue().toASCIIString();
						int lastIndex = scope.lastIndexOf('/') + 1;
						if (lastIndex != -1) {
							scope = scope.substring(lastIndex);
						}
					}
				} catch (MalformedURLException e) {
					this.logger.warn("Problem occurred while creating query beans.", e);
				}
			}
			QueryBean queryBean = new QueryBean(query, label, scope, operator);
			queryBeans.add(queryBean);
		} else if (query instanceof ComposedQuery) {
			ComposedQuery composedQuery = (ComposedQuery) query;
			//TODO NOT operator hack
			int index = 0;
			for (Query child : composedQuery.getQuery()) {
				Operator operatorToUse = composedQuery.getOperator();
				if (Operator.NOT.equals(operatorToUse) && index == 0 && composedQuery.getQuery().size() > 1){
					operatorToUse = Operator.AND;  // NOT operator hack
				}
				fillBeans(child, queryBeans, operatorToUse);
				index++;
			}
		}
	}
}
