/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.business.validator;

import org.ow2.weblab.portlet.business.bean.AdvancedSearchBean;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * A custom bean validator for the AdvancedSearchBean. 
 * @author emilienbondu
 *
 */
public class AdvancedSearchValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		boolean res = AdvancedSearchBean.class.equals(clazz);
		return res;
	}

	@Override
	public void validate(Object target, Errors errors) {
		AdvancedSearchBean bean = (AdvancedSearchBean) target;

		// TONOTDOFOROW2: add validation depending on the searcher implementation or a specific project on OW2
		// user may process any kind of document in any language
	
		// TODO process date properly knowing that incoming format is
		// yyy-mm-dd
		
		if (bean.getAndKeywords().isEmpty()
				& bean.getOrKeywords().isEmpty()
				& bean.getPhraseKeywords().isEmpty() 
				& bean.getNotKeywords().isEmpty() 
				& bean.getEndDate().isEmpty()
				& bean.getStartDate().isEmpty()
				& bean.getFormat().isEmpty()
				& bean.getLang().isEmpty()
				& bean.getEndDate().isEmpty()) {
			
			errors.reject("invalid.nokeywords");
		}
	}
}
