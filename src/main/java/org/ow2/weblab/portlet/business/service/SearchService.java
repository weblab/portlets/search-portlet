/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.service;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.services.searcher.SearchArgs;
import org.ow2.weblab.core.services.searcher.SearchReturn;
import org.ow2.weblab.portlet.business.bean.SearchBean;

/**
 * Service used by search portlet to manage user's search. It will work with an instance of {@link SearchBean}
 *
 * @author lmartin
 *
 * @param <T> search bean this service will use to make search
 */
public interface SearchService<T extends SearchBean> {

	/**
	 * Create a new SearchBean that will be use to manage user's queries during his session
	 * @param request
	 * @param response
	 *
	 * @return {@link SearchBean} a new SearchBean that will be use to manage user's queries during his session
	 */
	public T createSearchBean(PortletRequest request,PortletResponse response);

	/**
	 * Reset currently used search bean
	 * @param searchBean {@link SearchBean} current search bean to reset
	 */
	public void resetSearchBean(T searchBean);

	/**
	 * Prepare query according to current search bean fields values
	 *
	 * @param searchBean {@link SearchBean} current search bean
	 * @return {@link Query} corresponding query
	 * * @throws WebLabCheckedException if query could not be correctly prepared
	 */
	public Query prepareQuery(T searchBean) throws WebLabCheckedException;

	/**
	 * Update a search bean according to a Query
	 * @param searchBean {@link SearchBean} current search bean to update
	 * @param query
	 * @return {@link Query} new query corresponding to this updated search bean
	 * @throws WebLabCheckedException if query could not be correctly read
	 */
	public Query updateBean(T searchBean,Query query) throws WebLabCheckedException;

	/**
	 * Get init query used by this portlet when user's session is empty
	 *
	 * @param searchBean {@link SearchBean} current search bean
	 * @return {@link Query} corresponding query. If null ; nothing will append
	 */
	public Query getInitQuery(T searchBean);

	/**
	 * Make a search on search service and return corresponding result
	 *
	 * @param searchBean {@link SearchBean} current search bean
	 * @param searchArgs {@link SearchArgs} that will be used to execute this query (containing at least limit / offset propoerties)
	 * @return {@link SearchReturn} results of this search throws {@link WebLabCheckedException} if a exception happened during query execution
	 * @throws WebLabCheckedException
	 */
	public SearchReturn executeQuery(T searchBean, SearchArgs searchArgs) throws WebLabCheckedException;

	/**
	 * Test if a specific query is empty
	 *
	 * @param query {@link Query} query to test
	 * @return boolean true if query is empty else false
	 */
	public boolean isEmpty(Query query);

	/**
	 * Add a filter to current query (filter coming from facet for example)
	 *
	 * @param searchBean {@link SearchBean} current search bean
	 * @param scope {@link String} scope concerned by this filter (ie 'language')
	 * @param request {@link String} word corresponding to this filter (ie 'fr')
	 */
	public void addFilter(T searchBean, String scope, String request);

	/**
	 * Add a "not" filter to current query (filter coming from facet for example). A not filter will exclude all results matching filter
	 *
	 * @param searchBean {@link SearchBean} current search bean
	 * @param scope {@link String} scope concerned by this filter (ie 'language')
	 * @param request {@link String} word corresponding to this filter (ie 'fr')
	 */
	public void addNotFilter(T searchBean, String scope, String request);

	/**
	 * Remove a filter to current query (filter coming from facet for example)
	 *
	 * @param searchBean {@link SearchBean} current search bean
	 * @param scope {@link String} scope concerned by this filter (ie 'language')
	 * @param request {@link String} word corresponding to this filter (ie 'fr')
	 */
	public void removeFilter(T searchBean, String scope, String request);

	/**
	 * Remove a "not" filter to current query (filter coming from facet for example). A not filter will exclude all results matching filter
	 *
	 * @param searchBean {@link SearchBean} current search bean
	 * @param scope {@link String} scope concerned by this filter (ie 'language')
	 * @param request {@link String} word corresponding to this filter (ie 'fr')
	 */
	public void removeNotFilter(T searchBean, String scope, String request);

	/**
	 * Get last query stored in current search bean
	 *
	 * @param searchBean
	 * @return The last query fired
	 */
	public Query getLastQuery(T searchBean);

	/**
	 * Add an order property to order query (ASC/DSC) according to a specific property
	 *
	 * @param searchBean {@link SearchBean} current search bean
	 * @param orderProperty
	 * @param ascending
	 */
	public void addOrder(T searchBean, String orderProperty, boolean ascending);

}
