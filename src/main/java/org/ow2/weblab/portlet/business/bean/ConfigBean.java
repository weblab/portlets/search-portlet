/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.bean;

import org.springframework.stereotype.Service;

/**
 * Bean used to set configuration parameters used by search portlet
 *
 * @author lmartin
 *
 */
@Service(value = "configBean")
public class ConfigBean {

	private String searcherUserURI;
	private String searcherUsageContext;
	private String searcherServiceURI;

	private String analyserUserURI;
	private String analyserUsageContext;
	private String analyserServiceURI;

	private String titlePredicateResourceURI;
	private String descriptionPredicateResourceURI;
	private String formatPredicateResourceURI;
	private String isLinkedToPredicateResourceURI;
	private String hasGatheringDatePredicateResourceURI;
	// initial query for this service
	private String initalStringQuery;

	/**
	 * @return the searcherUserURI
	 */
	public String getSearcherUserURI() {
		return this.searcherUserURI;
	}

	/**
	 * @param searcherUserURI the searcherUserURI to set
	 */
	public void setSearcherUserURI(String searcherUserURI) {
		this.searcherUserURI = searcherUserURI;
	}

	/**
	 * @return the searcherUsageContext
	 */
	public String getSearcherUsageContext() {
		return this.searcherUsageContext;
	}

	/**
	 * @param searcherUsageContext the searcherUsageContext to set
	 */
	public void setSearcherUsageContext(String searcherUsageContext) {
		this.searcherUsageContext = searcherUsageContext;
	}

	/**
	 * @return the searcherServiceURI
	 */
	public String getSearcherServiceURI() {
		return this.searcherServiceURI;
	}

	/**
	 * @param searcherServiceURI the searcherServiceURI to set
	 */
	public void setSearcherServiceURI(String searcherServiceURI) {
		this.searcherServiceURI = searcherServiceURI;
	}

	/**
	 * @return the analyserUserURI
	 */
	public String getAnalyserUserURI() {
		return this.analyserUserURI;
	}

	/**
	 * @param analyserUserURI the analyserUserURI to set
	 */
	public void setAnalyserUserURI(String analyserUserURI) {
		this.analyserUserURI = analyserUserURI;
	}

	/**
	 * @return the analyserUsageContext
	 */
	public String getAnalyserUsageContext() {
		return this.analyserUsageContext;
	}

	/**
	 * @param analyserUsageContext the analyserUsageContext to set
	 */
	public void setAnalyserUsageContext(String analyserUsageContext) {
		this.analyserUsageContext = analyserUsageContext;
	}

	/**
	 * @return the analyserServiceURI
	 */
	public String getAnalyserServiceURI() {
		return this.analyserServiceURI;
	}

	/**
	 * @param analyserServiceURI the analyserServiceURI to set
	 */
	public void setAnalyserServiceURI(String analyserServiceURI) {
		this.analyserServiceURI = analyserServiceURI;
	}

	/**
	 * @return the titlePredicateResourceURI
	 */
	public String getTitlePredicateResourceURI() {
		return this.titlePredicateResourceURI;
	}

	/**
	 * @param titlePredicateResourceURI the titlePredicateResourceURI to set
	 */
	public void setTitlePredicateResourceURI(String titlePredicateResourceURI) {
		this.titlePredicateResourceURI = titlePredicateResourceURI;
	}

	/**
	 * @return the descriptionPredicateResourceURI
	 */
	public String getDescriptionPredicateResourceURI() {
		return this.descriptionPredicateResourceURI;
	}

	/**
	 * @param descriptionPredicateResourceURI the descriptionPredicateResourceURI to set
	 */
	public void setDescriptionPredicateResourceURI(String descriptionPredicateResourceURI) {
		this.descriptionPredicateResourceURI = descriptionPredicateResourceURI;
	}

	/**
	 * @return the formatPredicateResourceURI
	 */
	public String getFormatPredicateResourceURI() {
		return this.formatPredicateResourceURI;
	}

	/**
	 * @param formatPredicateResourceURI the formatPredicateResourceURI to set
	 */
	public void setFormatPredicateResourceURI(String formatPredicateResourceURI) {
		this.formatPredicateResourceURI = formatPredicateResourceURI;
	}

	/**
	 * @return the isLinkedToPredicateResourceURI
	 */
	public String getIsLinkedToPredicateResourceURI() {
		return this.isLinkedToPredicateResourceURI;
	}

	/**
	 * @param isLinkedToPredicateResourceURI the isLinkedToPredicateResourceURI to set
	 */
	public void setIsLinkedToPredicateResourceURI(String isLinkedToPredicateResourceURI) {
		this.isLinkedToPredicateResourceURI = isLinkedToPredicateResourceURI;
	}

	/**
	 * @return the hasGatheringDatePredicateResourceURI
	 */
	public String getHasGatheringDatePredicateResourceURI() {
		return this.hasGatheringDatePredicateResourceURI;
	}

	/**
	 * @param hasGatheringDatePredicateResourceURI the hasGatheringDatePredicateResourceURI to set
	 */
	public void setHasGatheringDatePredicateResourceURI(String hasGatheringDatePredicateResourceURI) {
		this.hasGatheringDatePredicateResourceURI = hasGatheringDatePredicateResourceURI;
	}

	/**
	 * @return the initalStringQuery
	 */
	public String getInitalStringQuery() {
		return this.initalStringQuery;
	}

	/**
	 * @param initalStringQuery the initalStringQuery to set
	 */
	public void setInitalStringQuery(String initalStringQuery) {
		this.initalStringQuery = initalStringQuery;
	}
}
