/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.ow2.weblab.portlet.business.bean.SearchBean;
import org.ow2.weblab.portlet.business.bean.SearchBeanImpl;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Validator used before Query launch. It check SearchBean is OK and will send a new search correctly
 *
 * @see SearchBean
 * @author lmartin
 *
 */
public class SearchValidator implements Validator {

	final static private SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	public boolean supports(Class<?> clazz) {
		return (clazz != null) && (SearchBean.class.isAssignableFrom(clazz));
	}

	@Override
	public void validate(Object target, Errors errors) {
		SearchBeanImpl searchBean = (SearchBeanImpl) target;

		// facet filters bound to this search bean
		boolean hasFilters = !CollectionUtils.isEmpty(searchBean.getFilters());
		boolean hasNotFilters = !CollectionUtils.isEmpty(searchBean.getNotFilters());

		// "advanced" search case
		if (searchBean.isAdvancedSearch()) {
			boolean hasAndWords = StringUtils.hasText(searchBean.getAndKeywords());
			boolean hasOrWords = StringUtils.hasText(searchBean.getOrKeywords());
			boolean hasPhraseWords = StringUtils.hasText(searchBean.getPhraseKeywords());
			boolean hasNotWords = StringUtils.hasText(searchBean.getNotKeywords());
			boolean hasFormat = StringUtils.hasText(searchBean.getFormat());
			String lang = searchBean.getLang();
			boolean hasLang = StringUtils.hasText(lang);
			boolean hasStartDate = StringUtils.hasText(searchBean.getStartDate());
			boolean hasEndDate = StringUtils.hasText(searchBean.getEndDate());

			// empty request case
			if ((!hasFilters) && (!hasNotFilters) && (!hasAndWords) && (!hasOrWords) && (!hasPhraseWords) && (!hasNotWords) && (!hasFormat) && (!hasLang) && (!hasStartDate) && (!hasEndDate)) {
				errors.reject("error.emptyQuery");
				return;
			}

			// check format
			// FIXME : why only these formats ?
			if (hasFormat && !(searchBean.getFormat().equals("pdf") || searchBean.getFormat().equals("xml") || searchBean.getFormat().equals("html") || searchBean.getFormat().equals("doc"))) {

				errors.rejectValue("format", "invalid.format");
			}

			if (hasLang && (lang.trim().length() != 2)) {
				errors.rejectValue("lang", "invalid.lang");
			}

			// check dates

			if (hasStartDate) {
				if (!hasEndDate) {
					System.err.println("DATES  empty end date");
					errors.rejectValue("endDate", "invalid.emptyDate");
				}
				try {
					DATE_FORMAT.parse(searchBean.getStartDate());
				} catch (ParseException e) {
					e.printStackTrace();
					errors.rejectValue("startDate", "invalid.dateFormat");
				}
			}
			if (hasEndDate) {
				if (!hasStartDate) {
					errors.rejectValue("startDate", "invalid.emptyDate");
				}
				try {
					DATE_FORMAT.parse(searchBean.getEndDate());
				} catch (ParseException e) {
					e.printStackTrace();
					errors.rejectValue("endDate", "invalid.dateFormat");
				}
			}

			// "normal" case
		} else {
			if ((!hasFilters) && (!hasNotFilters)) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "textQuery", "error.emptyTextQuery");
			}
		}
	}
}
