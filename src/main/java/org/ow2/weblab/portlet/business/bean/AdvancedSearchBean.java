/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.business.bean;

public class AdvancedSearchBean {

	private String andKeywords;
	private String orKeywords;
	private String notKeywords;
	private String phraseKeywords;
	private String lang;
	private String format;
	private String startDate;
	private String endDate;

	public AdvancedSearchBean() {
		super();
	}

	public String getAndKeywords() {
		return this.andKeywords;
	}

	public void setAndKeywords(String andKeywords) {
		this.andKeywords = andKeywords;
	}

	public String getOrKeywords() {
		return this.orKeywords;
	}

	public void setOrKeywords(String orKeywords) {
		this.orKeywords = orKeywords;
	}

	public String getNotKeywords() {
		return this.notKeywords;
	}

	public void setNotKeywords(String notKeywords) {
		this.notKeywords = notKeywords;
	}

	public String getPhraseKeywords() {
		return this.phraseKeywords;
	}

	public void setPhraseKeywords(String phraseKeywords) {
		this.phraseKeywords = phraseKeywords;
	}

	public String getLang() {
		return this.lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getFormat() {
		return this.format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getStartDate() {
		return this.startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return this.endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
}
