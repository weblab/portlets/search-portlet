/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.service;

import java.util.List;
import java.util.Map;

/**
 * Service used by search portlet to suggest terms to user while he is filling text input
 * 
 * @author lmartin
 * 
 */
public interface SuggestService {
	
	/**
	 * Suggest a list of strings matching input user is currently typing as input query
	 * @param inputName {@link String} input user is currently filling
	 * @param inputValue {@link String} corresponding value user is currently typing 
	 * @return {@link List} list of proposal find in index (text, entities...) matching user's current input (containing or suggest as correction).
	 * Elements are a key value map : key==displayed label, value==corresponding value
	 * 
	 */
	public List<Map<String, String>> suggest(String inputName,String inputValue);
}
