/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business;


/**
 * Just a place holder for constants.
 * 
 * @author lmartin
 */
public class Constants {
	
	//Session Beans names
	final static public String SEARCH_BEAN="searchBean";
	
	//Views
	final static public String MAIN_VIEW = "mainView";
	
	//Actions
	final static public String ACTION_PARAM="action";
	
	final static public String TEXT_QUERY_PARAM="textQuery";
	
	final static public String FILTER_PARAM="filter";
	
	final static public String FILTER_SCOPE_PARAM="filterScope";
	
	final static public String SEARCH_ACTION="search";
	
	final static public String SEARCH_ACTION_MAPPING=ACTION_PARAM+"="+SEARCH_ACTION;

	final static public String RESET_ACTION="reset";
	
	final static public String SAVE_ACTION="save";
	
	final static public String RESET_ACTION_MAPPING=ACTION_PARAM+"="+RESET_ACTION;
	
	final static public String SAVE_ACTION_MAPPING=ACTION_PARAM+"="+SAVE_ACTION;

	final static public String REMOVE_FILTER_ACTION="removeFilter";
	
	final static public String REMOVE_FILTER_ACTION_MAPPING=ACTION_PARAM+"="+REMOVE_FILTER_ACTION;

	final static public String REMOVE_FILTER_NOT_ACTION="removeNotFilter";
	
	final static public String REMOVE_FILTER_NOT_ACTION_MAPPING=ACTION_PARAM+"="+REMOVE_FILTER_NOT_ACTION;
	
	
	//Events
	final static public String QUERY_EVENT_QNAME="{http://weblab.ow2.org/events/search}query";
	
	final static public String SEARCH_EVENT_QNAME="{http://weblab.ow2.org/events/search}search";
	
	final static public String RESET_EVENT_QNAME="{http://weblab.ow2.org/events/search}reset";
	
	final static public String SAVE_EVENT_QNAME="{http://weblab.ow2.org/events/search}saveQuery";
	
	final static public String ADD_FACET_EVENT_QNAME="{http://weblab.ow2.org/events/facet}facetSelection";
	
	final static public String ADD_NOT_FACET_EVENT_QNAME="{http://weblab.ow2.org/events/facet}facetNotSelection";
	
	final static public String NEXT_RESULTS_EVENT_QNAME="{http://weblab.ow2.org/events/result}nextDocuments";
	
	final static public String NEXT_SEARCH_EVENT_QNAME="{http://weblab.ow2.org/events/search}next";
	
	final static public String ORDER_RESULTS_EVENT_QNAME="{http://weblab.ow2.org/events/result}orderDocuments";
	
	//auto suggest 
	//resource URL id
	final static public String SUGGEST_RESOURCE="suggestResource";
	//name of HTTP parameter used to identify input
	final static public String SUGGEST_CURRENT_INPUT_PARAM="suggestCurrentInput";
	//name of HTTP parameter used to send value currently typed by user
	final static public String SUGGEST_CURRENT_VALUE_PARAM="suggestCurrentValue";	
	
	
	//error msg HTTP param
	final static public String ERROR_PARAM="errorMsg";
	
	//name of resource bundle used in this portlet
	final static public String BUNDLE="search_portlet";
	
	//comments used as RDF comment in queries to identifiy each sub queries (used to read a prviously saved queries and update search bean according to them)
	final static public String QUERY_TEXT_COMMENT="textQuery";
	final static public String ADVANCED_COMMENT="advancedQuery";
	final static public String AND_WORDS_COMMENT="andWords";
	final static public String OR_WORDS_COMMENT="orWords";
	final static public String PHRASE_COMMENT="phrase";
	final static public String NOT_WORDS_COMMENT="notWords";
	final static public String FILTER_COMMENT="filter";
	final static public String NOT_FILTER_COMMENT="notFilter";
	final static public String LANG_COMMENT="langQuery";
	final static public String FORMAT_COMMENT="formatQuery";
	final static public String DATE_COMMENT="dateQuery";

}
