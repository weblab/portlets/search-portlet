<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<portlet:defineObjects />
<portlet:actionURL name="do_adv_search" windowState="normal" var="searchURL"/>

<portlet:renderURL var="normal_link_back" windowState="normal"/>


<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('input#startPicker,input#endPicker').datepick({ 
		onSelect: customRange, showTrigger: '#calImg', dateFormat: 'yyyy-mm-dd'});
	
	function customRange(dates) { 
	    if (this.id == 'startPicker') { 
	    	jQuery('input#endPicker').datepick('option', 'minDate', dates[0] || null); 
	    } 
	    else { 
	    	jQuery('input#startPicker').datepick('option', 'maxDate', dates[0] || null); 
	    } 
	}
});
</script>

<fmt:setLocale value="${pageContext.request.locale}" />
<fmt:setBundle basename="search_portlet" />


<div>
	<a id="advanced-link" href="${normal_link_back }"><fmt:message key="search.advanced.back" /></a>
</div>

<div id="advanced-form-title">
	<fmt:message key="advanced.find"/>
</div>

<div id="searchBox">	
	<form:form commandName="advancedSearchBean" method="POST" name="advancedSearchForm" action="${searchURL }">
		<form:errors path="*" cssClass="portlet-msg-alert" element="div" />	
		<table>
			<tr>
				<td><fmt:message key="advanced.andKeywords"/></td>
				<td><form:input id="advanced-andKeywords" path="andKeywords"/></td>
				<td>
					<font style="color:red" >
						<form:errors path="andKeywords"/>
					</font>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="advanced.phraseKeywords"/></td>
				<td><form:textarea id="advanced-phraseKeywords" path="phraseKeywords"/></td>
				<td>
					<font style="color:red" >
						<form:errors path="phraseKeywords"/>
					</font>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="advanced.orKeywords"/></td>
				<td><form:input id="advanced-orKeywords" path="orKeywords"/></td>
				<td>
					<font style="color:red" >
						<form:errors path="orKeywords"/>
					</font>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="advanced.notKeywords"/></td>
				<td><form:input id="advanced-notKeywords" path="notKeywords"/></td>
				<td>
					<font style="color:red" >
						<form:errors path="notKeywords"/>
					</font>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="advanced.format"/></td>
				<td><form:input id="advanced-format" path="format"/></td>
				<td>
					<font style="color:red" >
						<form:errors path="format"/>
					</font>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="advanced.lang"/></td>
				<td><form:input id="advanced-lang" path="lang"/></td>
				<td>
					<font style="color:red" >
						<form:errors path="lang"/>
					</font>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="advanced.date"/></td>
				<td><form:input id="startPicker" cssClass="advanced-date" path="startDate"/><fmt:message key="advanced.date.to"/><form:input id="endPicker" cssClass="advanced-date" path="endDate"/></td>
				<td>
					<font style="color:red" >
						<form:errors path="startDate"/><form:errors path="endDate"/>
					</font>
				</td>
			</tr>
		</table>
		<input type="submit" name="search_submit" value="<fmt:message key="search.button" />" />
	</form:form>
</div>
