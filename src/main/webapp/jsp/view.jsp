<%@page import="org.ow2.weblab.portlet.business.Constants"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<portlet:defineObjects />
<fmt:setLocale value="${pageContext.request.locale}" />
<fmt:setBundle basename="search_portlet" />

<portlet:renderURL var="advanced_link" windowState="maximized">
	<portlet:param name="action" value="go_advanced_search"/>
</portlet:renderURL>

<portlet:resourceURL var="autoCompleteURL" id="<%=Constants.SUGGEST_RESOURCE%>" /> 

<script type="text/javascript">
	function removeFilter(queryUri) {
		if (queryUri.length > 0 && queryUri.length > 0) {
			jQuery('form#do_remove_filter input#query-uri').val(queryUri);
			jQuery('form#do_remove_filter input#remove-rank').val(jQuery('form#do_search_form select#rank_search_form').val());
			jQuery('form#do_remove_filter input#remove-scope').val(jQuery('form#do_search_form select#scope_search_form').val());
			jQuery('form#do_remove_filter').submit();
		}
	};
	
	function checkQueryAndLaunch(){
		var q = jQuery('input#searchBox_input').val();
		if(q == null || q == "" || q == "<fmt:message key="search.defaultText" />"){
			alert('<fmt:message key="search.emptyQueryalert" />');
		}else{
			jQuery('form#do_search_form').submit();
		}
	};
	
</script>

<script src="<%=request.getContextPath()%>/js/jquery.autocomplete.js"></script>


<c:if test="${error}">
	<div class="portlet-msg-error">
		<fmt:message key="portlet.error" /> : ${error_message}
	</div> 
</c:if>

<div id="searchBox" class="contenu_portlet">
	<div id="advanced-search">
		<a id="advanced-link" href="${advanced_link }"><fmt:message key="search.advanced" /></a>
	</div>
	<form id="do_search_form" method='POST' action="<portlet:actionURL name="do_search" />">
		<c:choose>
			<c:when test="${empty user_last_search_beans}">
				<input id="searchBox_input" type="text" name="queryInput" value="<fmt:message key="search.defaultText" />"  size="100" />
			</c:when>
			<c:otherwise>
				<input id="searchBox_input" type="text" name="queryInput" value="" title="<fmt:message key="search.tooltip" />" size="100" />
			</c:otherwise>
		</c:choose>
		<input type="button" name="search_submit" value="<fmt:message key="search.button" />" onclick="checkQueryAndLaunch();" />
	</form>
	
	<div id="advanced-buttons">
		<form id="do_reset_form" method='POST' action="<portlet:actionURL name="do_reset"/>">
			<input type="submit" name="reset_submit" value="<fmt:message key="reset.button" />" />
		</form>
		<c:if test="${not empty user_last_search_beans}">
			<form id="do_save_form" method='POST' action="<portlet:actionURL name="do_save"/>">
				<input type="submit" name="save_submit" value="<fmt:message key="save.button" />" />
			</form>
		</c:if>
	</div>
</div>

<form id="do_remove_filter" method='POST' action="<portlet:actionURL name="remove_filter"/>">
	<input type="hidden" id="query-uri" name="queryUri">
	<input type="hidden" id="remove-scope" name="scopeInput">
	<input type="hidden" id="remove-rank" name="rankingInput">
</form>

<div id="user-filter-list">
	<c:forEach var="queryBean" items="${user_last_search_beans}" varStatus="status">
		<div class="search-filter filter-operator-${queryBean.operator}">
			<c:if test="${not empty queryBean.scope}">
				<span class="scope">${queryBean.scope} : </span>
			</c:if>
			${queryBean.label} <span class="cross" onclick="removeFilter('${queryBean.query.uri}');" title="<fmt:message key="search.querybean"/>"> X </span>
		</div>
	</c:forEach>
	<c:if test="${not empty user_last_search_beans}">
		<div class="search-filter"><span class="cross" onclick="jQuery('form#do_reset_form').submit();" title="<fmt:message key="reset.button" />"> X </span></div>
	</c:if>
</div>

<script>

jQuery(document).ready(function(){
		jQuery('input#searchBox_input').autocomplete("${autoCompleteURL}", 
				{ minChars:3, queryParamName:"<%=Constants.SUGGEST_CURRENT_VALUE_PARAM%>", 
					extraParams:{<%=Constants.SUGGEST_CURRENT_INPUT_PARAM%>:"test"}, 
					useCache:false, matchSubset:false, matchInside:false, filter:false, remoteDataType:"json", sortResults:false});
		
		jQuery('#searchBox_input').attr('value','');
		jQuery('#searchBox_input').focus();
});

</script>